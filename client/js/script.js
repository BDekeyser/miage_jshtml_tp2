window.onload = init;

function init() {
    new Vue({
        el: "#app",
        data: {
            restaurants: [{
                    nom: 'café de Paris',
                    cuisine: 'Française'
                },
                {
                    nom: 'Sun City Café',
                    cuisine: 'Américaine'
                }
            ],
            page: 1,
            name: '',
            nom: '',
            cuisine: '',
            nbRestaurants: 0
        },
        mounted() {
            this.getRestaurantsFromServer();
        },
        methods: {

            getRestaurantsFromServer() {
                let url = 'http://localhost:8080/api/restaurants?page=' + this.page + '&name=' + this.name;
                fetch(url)
                    .then((responseJSON) => {
                        return responseJSON.json();
                    })
                    .then((responseJS) => {
                        this.restaurants = responseJS.data;
                        this.nbRestaurants = responseJS.count;
                    })
                    .catch((err) => {
                        console.log('error', err)
                    })
            },
            supprimerRestaurant(restaurant) {
                let id = restaurant._id;
                let url = "http://localhost:8080/api/restaurants/" + id;

                fetch(url, {
                        method: "DELETE",
                    })
                    .then((responseJSON) => {
                        responseJSON.json()
                    })
                    .then((res) => {
                        this.getRestaurantsFromServer();
                    })
                    .catch(function (err) {
                        console.log(err);
                    });
            },
            ajouterRestaurant(event) {
                console.log(event)
                event.preventDefault();
                let form = event.target;
                let donneesFormulaire = new FormData(form);
                let url = "http://localhost:8080/api/restaurants";

                fetch(url, {
                        method: "POST",
                        body: donneesFormulaire
                    })
                    .then((responseJSON) => {
                        responseJSON.json()
                    })
                    .then((res) => {
                        this.getRestaurantsFromServer();
                    })
                    .catch(function (err) {
                        console.log(err);
                    });
            },
            modifierRestaurant(event) {
                event.preventDefault();
                let form = event.target;
                let donneesFormulaire = new FormData(event.target);
                let id = form._id.value;
                let url = "/api/restaurants/" + id;

                fetch(url, {
                        method: "PUT",
                        body: donneesFormulaire
                    })
                    .then((responseJSON) => {
                        responseJSON.json()
                    })
                    .then((res) => {
                        this.getRestaurantsFromServer();
                    })
                    .catch(function (err) {
                        console.log(err);
                    });
            },
            getColor(index) {
                return (index == 4) ? 'pink' : 'orange';
            },
            pageSuivante() {
                if (this.page < this.getMaxPage()) {
                    this.page++;
                    this.getRestaurantsFromServer();
                }
            },
            pagePrecedente() {
                if (this.page > 1) {
                    this.page--;
                    this.getRestaurantsFromServer()
                }
            },
            premierePage() {
                if (this.page != 1) {
                    this.page = 1;
                    this.getRestaurantsFromServer()
                }
            },
            dernierePage() {
                if (this.page != this.getMaxPage()) {
                    this.page = this.getMaxPage();
                    this.getRestaurantsFromServer()
                }
            },
            getMaxPage() {
                return Math.floor(this.nbRestaurants / 10);
                // On a 10 résultats par page donc /10
                // On arrondit à l'entier inférieur puisque pour 25349 restaurants il faut 2534 pages
            }
        }
    })
}